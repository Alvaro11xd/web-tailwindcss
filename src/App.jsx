function App() {
  return (
    <>
      <div className="container min-h-screen bg-[url('../public/assets/images/image.png')] bg-center bg-cover px-28 py-5 relative">
        <nav className="flex items-center">
          <img
            src="../public/assets/images/logo.png"
            alt="logo"
            className="w-40 cursor-pointer"
          />
          <ul className="flex-1 text-center">
            <li className="list-none inline-block px-5">
              <a href="#" className="no-underline text-white px-5">
                Home
              </a>
            </li>
            <li className="list-none inline-block px-5">
              <a href="#" className="no-underline text-white px-5">
                About
              </a>
            </li>
            <li className="list-none inline-block px-5">
              <a href="#" className="no-underline text-white px-5">
                Features
              </a>
            </li>
            <li className="list-none inline-block px-5">
              <a href="#" className="no-underline text-white px-5">
                Contact
              </a>
            </li>
          </ul>
          <img
            src="../public/assets/images/cart.png"
            alt="icono"
            className="w-8 cursor-pointer"
          />
        </nav>

        <div className="text-white mt-48 max-w-xl">
          <h1 className="text-6xl font-semibold leading-normal">
            Jessica
            <br />
            delivery in <span className="font-light">15 mins</span>
          </h1>
          <p>
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Harum eius
            ducimus blanditiis hic laudantium consequuntur? Et est quae
            molestias vel!
          </p>

          <div className="mt-10">
            <a
              href="#"
              className="bg-yellow-300 rounded-3xl py-3 px-8 font-medium inline-block mr-4 hover:bg-transparent hover:border-yellow-300 hover:text-white duration-300 hover:border border border-transparent">
              Order now
            </a>
            <a href="#">
              Download app
              <span className="text-lg inline-block rotate-90">&#10148;</span>
            </a>
          </div>
        </div>

        <img
          src="../public/assets/images/grocery-image.png"
          className="w-full xl:w-1/2 xl:absolute bottom-0 right-20"
        />
      </div>
    </>
  );
}

export default App;
